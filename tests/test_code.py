"""
Wow, such good tests
"""

import unittest

from src.not_ok import bad_multiply
from src.ok import good_multiply


class TestCode(unittest.TestCase):
    def test_should_raise_exception(self) -> None:
        with self.assertRaises(Exception) as context:
            bad_multiply(0, 0, 0, 0, "e")
        self.assertEqual("unsupported operand type(s) for /: 'str' and 'int'", str(context.exception))

    def test_should_return_data(self) -> None:
        multiplication = good_multiply(1, 1, 1, 1, 1)
        self.assertEqual(1, multiplication)
